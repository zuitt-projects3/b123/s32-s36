const express = require('express');
const mongoose = require('mongoose');
/*
  Cors is a package used a middleware so that we can approve
  the use of our routes and controllers by other applications
  such our front-end applications
*/
const cors = require('cors');
const app = express();
const port = process.env.PORT || 4000;
app.use(express.json());
// course Route
const courseRoutes = require('./routes/courseRoutes');
console.log(courseRoutes);
app.use('/courses',courseRoutes);

// user Route
const userRoutes = require('./routes/userRoutes');
console.log(userRoutes);
app.use('/users', userRoutes)

mongoose.connect("mongodb+srv://princebarro:Arnaments123@cluster0.lw2dr.mongodb.net/bookingAPI?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(express.json());
app.listen(port, ()=>console.log(`Server running at port ${port}`))
