const User = require('../models/User');
const Course = require('../models/Course')
const bcrypt = require('bcrypt');
// bcrypt has methods which will help us add a layer of security in our password

// import the auth module and deconstruct to get our createAccessToken method
const auth = require('../auth')
const {createAccessToken} = auth;
module.exports.createNewUser = (req,res) => {
  console.log(req.body);
if(req.body.password.length < 8) return res.send({message: "Password too Short"})
  /*
    What bcrypt does is hash our password string into randomized character
    version of your password.

    It is able to hide your password within that randomized string.

    syntax:
    bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

    Salt-Rounds are the number of times the characters in the hash are
    randomized.
    -the higher the salt rounds the more secure your password but it will
    eat a lot of memory in your application
  */
  const hashedPW = bcrypt.hashSync(req.body.password,10)
  console.log(hashedPW);

   let newUser = new User ({
     firstName: req.body.firstName,
    lastName: req.body.lastName,
    mobileNo: req.body.mobileNo,
    email: req.body.email,
    password: hashedPW
  })
  newUser.save()
  .then(registeredUser => res.send(registeredUser))
  .catch(err => res.send(err))
}

module.exports.loginUser = (req,res) => {
  /*
    4 steps to do in login:

    1. Find the user by the email
    2. if there is a found user we will check the password.
    3. if there is no found user we will send a message to the client instead
    4. If upon checking the password is correct we will generate a key to
    access our app. If not we will the client away by sending a message to
    the client.
  */
  User.findOne({email: req.body.email})
  .then(result => {
    if(result === null){
      return res.send({message: "No User Found"})
    } else {
      console.log(req.body.email);
      console.log(req.body.password);

      const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password)
      /*
        bcrypt.compareSync(<string>,<hasedString>)

        returns a boolean after comparing the first argument (string) and the
        hashed version of that string. if it matches it returns a true
      */
      console.log(isPasswordCorrect);
      if(isPasswordCorrect){
        // console.log("We will generate a key.")
        return res.send({accessToken: createAccessToken(result)});
      } else {
        return res.send({message: "Password Incorrect"})
      }
    }
  })

  .catch(err => res.send(err));

}

module.exports.getSingleUser = (req,res) => {

  // logged in user's details after decoding with auth module's verify()
  console.log(req.user);
  User.findById(req.user.id)
  .then(result => res.send(result))
  .catch(err => res.send(err));
}

module.exports.updateProfile = (req,res) => {

// we can get the id of the logged user through req.user
  console.log(req.user);
// find the user by it's id and update it
let updates = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    mobileNo: req.body.mobileNo,
  }
  User.findByIdAndUpdate(req.user.id,updates,{new: true})
  .then(updatedUser => res.send(updatedUser))
  .catch(err => res.send(err))
}

/*
  async and await:

  With the use of async and await we can wait for the process to finish before we can move forward.

  await can only be used in async function
*/

module.exports.enroll = async (req,res) => {
  // console.log(req.user.id);
  // console.log(req.body.courseID);
  if(req.user.isAdmin) return res.send({
    auth: "Failed",
    message: "Action Forbidden"
  });
  /*
    Enrollment steps:
    1.Look for our user.
      Push the details of the course we're trying to enroll in our user's subdocument array. .save() the user document and return true to a variable if saving is successful, return the error message if we catch an error.
    2.Look for our course.
      Push the details of the user we're trying to enroll in our courses's enrollee's subdocument array. .save() the user document and return true to a variable if saving is successful, return the error message if we catch an error.
    3. When both saving of documents are successful, we send a message to the client

  */
  let isUserUpdated = await User.findById(req.user.id)
  .then(user => {
    // console.log(user.enrollments)

    // Add the courseID in the user's enrollment array
    user.enrollments.push({courseId: req.body.courseId})

    // return the value of saving the document
    return user.save()
    .then(user => true)
    .catch(err => err.message)
  })
  console.log(isUserUpdated);
  // End the request/response when isUserUpdated does not return true
  if(isUserUpdated !== true) return res.send(isUserUpdated);

  let isCourseUpdated = await Course.findById(req.body.courseId)
  .then(course => {
    // Add the logged in user's id into the enrolles subdocument array:
    course.enrollees.push({userId: req.user.id})
    return course.save()
    .then(user => true)
    .catch(err => err.message)
  })
  console.log(isCourseUpdated);
  if(isCourseUpdated !== true) return res.send(isCourseUpdated);
  if(isUserUpdated && isCourseUpdated) return res.send("Enrolled Succesfully")
}

// check checkEmail
module.exports.checkEmail = (req,res) => {
  User.findOne({email: req.body.email})
  .then(result => {
    if(result === null){
      return res.send({message: "Email Available"})
    } else {
      return res.send({
        isAvailable: false,
        message: "Email already Registered"

      })
    }
  })
  .catch(err => res.send(err))
}

module.exports.getEnrollments = (req,res) => {
  console.log(req.user.id)
  User.findById(req.user.id)
  .then(result => res.send(result.enrollments))

  .catch(err => res.send(err))
}
