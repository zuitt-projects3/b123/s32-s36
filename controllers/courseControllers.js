const Course = require('../models/Course');

module.exports.createCourse = (req,res) => {
  console.log(req.body)
  let newCourse = new Course ({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  })
  newCourse.save()
  .then(registeredCourse => res.send(registeredCourse))
  .catch(err => res.send(err))
}

module.exports.findCoursesDocuments = (req,res) => {
  Course.find({})
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

module.exports.findActiveDocuments = (req,res) => {
  Course.find({isActive: true})
  .then(result => res.send(result))
  .catch(err => res.send(err));
}

module.exports.findSingleCourse = (req,res) => {
  console.log(req.params.id)
  Course.findById(req.params.id)
  .then(result => res.send(result))
  .catch(err => res.send(err))
}

// updateCourse
module.exports.updateCourse = (req,res) => {
  console.log(req.params.id);
  let updates = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }
  Course.findByIdAndUpdate(req.params.id,updates,{new: true})
  .then(updatedCourse => res.send(updatedCourse))
  .catch(err => res.send(err))
}

// archive Course
module.exports.archiveCourse = (req,res) => {
  console.log(req.params.id)
  let updates = {
    isActive: false
  }
  Course.findByIdAndUpdate(req.params.id,updates,{new: true})
  .then(archivedCourse => res.send(archivedCourse))
  .catch(err => res.send(err))
}

// activate course
module.exports.activateCourse = (req,res) => {
  console.log(req.params.id)
  let updates = {
    isActive: true
  }
  Course.findByIdAndUpdate(req.params.id,updates,{new: true})
  .then(activatedCourse => res.send(activatedCourse))
  .catch(err => res.send(err))
}

module.exports.getEnrollees = (req,res) => {
  console.log(req.params.id)
  Course.findById(req.params.id)
  .then(result => res.send(result.enrollees))
  .catch(err => res.send(err))
}
