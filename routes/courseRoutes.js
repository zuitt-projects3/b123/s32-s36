 const express = require("express");
 const router = express.Router();
 const auth = require('../auth');
 const {verify,verifyAdmin} = auth;
 const courseControllers = require('../controllers/courseControllers');
 const {
   createCourse,
   findCoursesDocuments,
   findActiveDocuments,
   findSingleCourse,
   updateCourse,
   archiveCourse,
   activateCourse,
   getEnrollees
 } = courseControllers;

// create new course
router.post('/',verify,verifyAdmin,createCourse);

router.get('/',verify,verifyAdmin,findCoursesDocuments);

router.get('/getActiveCourses',verify,findActiveDocuments);

router.get('/getSingleCourse/:id',verify,findSingleCourse);

// update course documents
router.put('/:id',verify,verifyAdmin,updateCourse)
// archive course documents
router.put('/archive/:id',verify,verifyAdmin,archiveCourse)
// activate course
router.put('/activate/:id',verify,verifyAdmin,activateCourse)

router.get('/getEnrollees/:id',verify,verifyAdmin,getEnrollees)
 module.exports = router;
