const express = require("express");
const router = express.Router();
const auth = require('../auth');
// verify is a method from our auth module which will let us verify if the jwt is legitimate and let us decode the payload.
const{verify} = auth;
const userControllers = require('../controllers/userControllers');
const {
  createNewUser,
  loginUser,
  getSingleUser,
  updateProfile,
  enroll,
  checkEmail,
  getEnrollments
} = userControllers

router.post('/',createNewUser)
router.post('/login',loginUser)

// route methods, much like middleware, give access to req,res and next object for the functions that are included in them.
// In fact, in expressJS, we can multiple layers of middleware to do the tasks before letting another function perform another task.
router.get('/getUserDetails',verify,getSingleUser)


// update the logged in user
router.put('/updateProf',verify,updateProfile)

// enroll rote
router.post('/enroll',verify,enroll)

// check email exist route
router.post('/checkEmail',checkEmail)


// get the enrollments of a logged in user
router.get('/getEnrollments',verify,getEnrollments)
module.exports = router;
