/*
  We wil create a module which will have functions and methods that will help
  us authenticate our users to either give them permission or restrict them
  from an action. To do this, first we need to give our users a key to access
  our app.
*/

const jwt = require('jsonwebtoken');
const secret = "CourseBookingApi";
/*
    JWT is a way to securely pass information from a part of a server to the
    frontend or other parts of the server. It allows us to create a sort of
    "keys" which is able to authenticate the user.

    The secret passed is any string but to allow access with the token, the
    secret must be intact.

    JWT is like a gift wrapping service but with a secret but only the system
    knows about the secret. And if the secret is not intact or the jwt seems tampered with we will be able to reject the user who used an illegitimate token.

    This will ensure that only registered users can do certain things in our app.
*/
module.exports.createAccessToken = (user) => {
  // console.log(user);

  // data object is created to contain some details of our users
  // Only a logged in user is handed or given the token.
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  // create your jwt with the payload, with the secret, the algorithm to create your JWT.
  return jwt.sign(data,secret,{});
}

module.exports.verify = (req,res,next) => {
  // token passed as bearer tokens can be found
  // console.log(req.headers.authorization);

  // token variable will hold our bearer token from our client
  let token = req.headers.authorization
  // console.log(token);

  if(typeof token === "undefined"){
    return res.send({auth: "Failed. No Token"});
  } else {
    // extract the token and remove the word "Bearer" from our token variable
    token = token.slice(7,token.length);
    // console.log(token);

    // verify the legitimacy of the token
    jwt.verify(token,secret,function(err,decodedToken){

      // err will contain the error from our decoding our token
      // decoded token is our token afrer completing and accomplishing verification or its legitimacy against our secret.
      if(err){
        return res.send({
          auth: "Failed",
          message: err.message
        })
      } else {
        console.log(decodedToken); //contains the data payload from our token
        // we will add a new property in the req object called user, then assign the decoded data to that property
        req.user = decodedToken;
        // next() will allow us to run the next function. (another Midlleware or the controller)
        next()
      }
    })
  }
}

module.exports.verifyAdmin = (req,res,next) => {
  if(req.user.isAdmin){
    next()
  } else {
    return res.send({
      auth: "Failed",
      message: "Action Forbidden"
    })
  }
}
